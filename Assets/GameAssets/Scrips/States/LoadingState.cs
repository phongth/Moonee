using System;
using Base.Pattern;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Game
{
    public class LoadingState : GameState
    {

        [SerializeField] private Camera loadingCam;
        [SerializeField] private Image loadingImage;

        public override void EnterStateBehaviour(float dt, GameState fromState)
        {
            loadingCam.gameObject.SetActive(true);
            loadingImage.color = new Color(0, 0, 0, 255f);
            //LoadAnim(0, 255).Forget();
            if (GameManager.GameLevel == 1)
            {
                LoadSceneAndSwitchState().Forget();
            }
            else
            {
                UnloadSceneAsync().Forget();
            }
        }

        public override void UpdateBehaviour(float dt)
        {
            
        }

        public override void CheckExitTransition()
        {
            if (GameManager.StateParam.loadingStateComplete) GameStateController.EnqueueTransition<MainState>();
        }

        public override void ExitStateBehaviour(float dt, GameState toState)
        {
            loadingCam.gameObject.SetActive(false);
            //DOTween.Kill(loadingImage);
            loadingImage.color = new Color(0, 0, 0, 0);
            GameManager.StateParam.loadingStateComplete = false;
            SceneManager.SetActiveScene(SceneManager.GetSceneAt(1));
        }

        private async UniTask LoadSceneAndSwitchState()
        {
            await LoadScene("Game");
            await LoadScene("UIScene");

            GameManager.StateParam.loadingStateComplete = true;
        }

        private async UniTaskVoid UnloadSceneAsync()
        {
            await UniTask.WhenAll(UnloadScene("Game"), UnloadScene("UIScene"));
            await UniTask.Delay(TimeSpan.FromSeconds(.5f));
            await LoadSceneAndSwitchState();
        }

        private async UniTask UnloadScene(string sceneName)
        {
            var scene = SceneManager.GetSceneByName(sceneName);
            if (scene.isLoaded)
            {
                await SceneManager.UnloadSceneAsync(sceneName).ToUniTask();
            }
            else await UniTask.Yield();
        }

        private async UniTask LoadScene(string sceneName)
        {
            await SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive).ToUniTask();
        }

        private async UniTask LoadAnim(int startAlpha, int endAlpha)
        {
            loadingImage.color = new Color(0, 0, 0, startAlpha);
            await loadingImage.DOFade(endAlpha, 1f).SetEase(Ease.InOutBack).AsyncWaitForCompletion();
        }
    }
}

