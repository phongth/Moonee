using System.Collections;
using System.Collections.Generic;
using Base;
using Base.MessageSystem;
using Base.Module;
using Base.Pattern;
using UnityEngine;

namespace Game
{
    public class MainState : GameState
    {
        public override void EnterStateBehaviour(float dt, GameState fromState)
        {
            GameManager.StateParam.isAllowInput = false;
        }

        public override void UpdateBehaviour(float dt)
        {
            if (GameManager.StateParam.isAllowInput)
            {
                Messenger.RaiseMessage(SystemMessage.Input, GameStateController.InputAction.Phase, GameStateController.InputAction.Position);
            }
            else
            {
                Messenger.RaiseMessage(SystemMessage.Input, InputPhase.Ended, Vector3.zero);
            }
        }

        public override void CheckExitTransition()
        {
            if (GameManager.StateParam.mainStateComplete) GameStateController.EnqueueTransition<LoadingState>();
        }

        public override void ExitStateBehaviour(float dt, GameState toState)
        {
            GameManager.StateParam.mainStateComplete = false;
            #if !UNITY_EDITOR
            SaveData save = new SaveData { gameLevel = GameManager.GameLevel, isVibrate = Setting.isVibrate };
            SaveLoad.SaveToBinary(save, "save.bin");
            #endif
        }
    }
}

