using System;
using System.Collections;
using System.Collections.Generic;
using Base;
using UnityEngine;

namespace Game
{
    public class LookAtCamera : BaseMono
    {
        private Camera _mainCamera;
        private RectTransform rectTransform;

        private void Start()
        {
            if (Camera.main)
            {
                _mainCamera = Camera.main;
            }

            rectTransform = GetComponent<RectTransform>();
        }

        private void LateUpdate()
        {
            if (Active)
            {
                // rectTransform.LookAt(_mainCamera.transform);
                // rectTransform.eulerAngles = new Vector3(rectTransform.eulerAngles.x * -1f, rectTransform.eulerAngles.y - 180f, 0);
                
                transform.LookAt(transform.position + _mainCamera.transform.rotation * Vector3.forward, _mainCamera.transform.rotation * Vector3.up);
            }
        }
    }
}

