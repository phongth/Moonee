using System;
using System.Collections;
using System.Collections.Generic;
using SensorToolkit;
using UnityEngine;
using TMPro;
using Base;
using Base.Pattern;
using DG.Tweening;

namespace Game
{
    public class Barrack : BuildingController
    {
        [Header("Output Grid Setting")]
        [SerializeField] private int width;
        [SerializeField] private int height;
        [SerializeField] private float padding;
        [SerializeField] private Transform gridRoot;

        [Header("Troops property")] 
        [SerializeField] private CharacterProperty defaultTroopProperty;

        private CharacterAttribute _crrTroopAttribute;
        
        private GridBase _gridBase = new GridBase(0, 0, 0);
        private float _timeElapsed;
        
        /// <summary>
        /// Condition of ruby production:
        /// </summary>
        /// <list type="Conditions">
        /// <item><see cref="Conditions.IsTimeRun"/> The time condition </item>
        /// <item><see cref="Conditions.IsFullUp"/> Full-up condition: if the grid is full of ruby, return true </item>
        /// </list>
        private Conditions _condition = new Conditions(false, false);
        private Queue<int> _produceQueue = new Queue<int>();
        private int _crrGridIndex = 0;
        
        protected override void Start()
        {
            base.Start();

            _gridBase = new GridBase(width, height, padding);

            for (int i = 0; i < width * height; ++i)
            {
                _gridBase.Add(0);
            }
            
            ObjectPooler.InitObjectPool("Troop");

            _crrTroopAttribute = new CharacterAttribute();
            _crrTroopAttribute.Init(defaultTroopProperty.Attribute);
            
            UpdateText();
        }
        
        private void Update()
        {
            if (_condition.IsTimeRun && !_condition.IsFullUp)
            {
                _timeElapsed -= Time.deltaTime;

                if (_timeElapsed <= 0)
                {
                    _timeElapsed = PropertyAttribute.productionSpeed.StatValue;
                    OutputTroops();
                }
            }
        }

        private void OnDrawGizmos()
        {
            if (_gridBase != null && !Application.isPlaying)
            {
                _gridBase = new GridBase(width, height, padding);
                
                for (int x = 0; x < width; ++x)
                {
                    for (int y = 0; y < height; ++y)
                    {
                        Vector3 position = _gridBase.GetWorldPosition(x, y);
                        Gizmos.DrawIcon(gridRoot.TransformPoint(new Vector3(position.x, 0, position.y)),
                            "BaseObjectSpawner/spawner_icon.png", true);
                    }
                }
            }
        }

        private void OutputTroops()
        {
            //_condition.IsTimeRun = false;
            if (_produceQueue.Count > 0)
            {
                int index = _produceQueue.Dequeue();
                PropertyAttribute.amountInRuby.StatValue++;
                PropertyAttribute.amountInRuby.StatValue =
                    Mathf.Clamp(PropertyAttribute.amountInRuby.StatValue, 0, DefaultAttribute.amountInRuby.StatValue * PropertyAttribute.buildingLevel.StatValue);

                Vector2 pos = _gridBase.Get(index);
                Vector3 pos2 = _gridBase.GetWorldPosition((int)pos.y, (int)pos.x);
                Vector3 position = gridRoot.TransformPoint(new Vector3(pos2.x, 0, pos2.y));

                //troop.SetTargetPosition(position);
                var troop = ObjectPooler.Get("Troop", position, Vector3.zero, SceneParent).GetComponent<AllyController>();
                troop.SetAttribute(_crrTroopAttribute);

                troop.AddListener(OnTroopTerminated);

                if (GameManager.GameLevel == 3)
                {
                    LevelController.CurrentObjective.DoObjective();
                }
                
                int indexGrid = _gridBase.GridList.FindIndex(item => item == 1);
                _gridBase.SetValue(indexGrid, troop.InstanceId);

                int countRubyProduce = _gridBase.GridList.FindAll(item => item != 0 && item != 1).Count;
                if (countRubyProduce >= PropertyAttribute.amountOut.StatValue)
                {
                    _condition.IsFullUp = true;
                    _timeElapsed = PropertyAttribute.productionSpeed.StatValue;
                }
                
                //UpdateText();
            }
        }

        private void OnTroopTerminated(AllyController troop)
        {
            int indexOfTroop = _gridBase.GetIndex(troop.InstanceId);
            if (indexOfTroop >= 0) _gridBase.SetValue(indexOfTroop, 0);

            troop.RemoveListener(OnTroopTerminated);
            ObjectPooler.Return("Troop", troop.CacheTransform);
        }
        
        protected override void OnDropCurrency(int instanceId, CurrencyType type)
        {
            if (instanceId != InstanceId || PropertyAttribute.amountInRuby.StatValue <= 0) return;

            PropertyAttribute.amountInRuby.StatValue--;
            _crrGridIndex = _gridBase.GridList.FindIndex(item => item == 0);
            _gridBase.SetValue(_crrGridIndex, 1);
            _produceQueue.Enqueue(_crrGridIndex);

            if (_produceQueue.Count > 0)
            {
                _condition.IsTimeRun = true;
                _timeElapsed = PropertyAttribute.productionSpeed.StatValue;
            }

            UpdateText();
        }
        
        public override void OnUpgradeCallback()
        {
            UpdateText();
        }

        public override void LevelUp()
        {
            base.LevelUp();
            
            _crrTroopAttribute.Upgrade();
        }
    }
}

