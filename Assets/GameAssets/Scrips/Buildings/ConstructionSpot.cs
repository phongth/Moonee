using System;
using Base;
using Base.MessageSystem;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using SensorToolkit;
using TMPro;
using UnityEngine;

namespace Game
{
    public class ConstructionSpot : BaseMono
    {
        private int _crrRequireCrystal;
        private int _crrRequireRuby;

        public int RequireCrystal => _crrRequireCrystal;

        public int RequireRuby => _crrRequireRuby;

        [SerializeField] private BuildingType buildingType;
        [SerializeField] private int requireCrystal;
        [SerializeField] private int requireRuby;
        [SerializeField] private BuildingController building;
        [SerializeField] private RequirementPopup popup;
        [SerializeField] private Transform sceneParent;

        private RequirementPopup _requirementPopup;
        public BuildingType BuildingType => buildingType;

        private void Awake()
        {
            _requirementPopup = Instantiate(popup, GameObject.FindGameObjectWithTag("Main Canvas").transform);
        }

        private void Start()
        {
            UpdateText();
            
            Messenger.RegisterListener<int, CurrencyType>(GameMessage.DropCurrencyConstruction, OnDropCurrency);
        }

        private void OnEnable()
        {
            _requirementPopup.SetTarget(CacheTransform, Vector3.zero);
            _requirementPopup.Active = true;
            _crrRequireCrystal = requireCrystal;
            _crrRequireRuby = requireRuby;
            
            UpdateText();
        }

        private void OnDisable()
        {
            if (_requirementPopup) _requirementPopup.Active = false;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            
            Messenger.RemoveListener<int, CurrencyType>(GameMessage.DropCurrencyConstruction, OnDropCurrency);
        }

        public async UniTask ConditionsAreMatch()
        {
            var buildingController = Instantiate(building, Position, building.Rotation, sceneParent);
            buildingController.SceneParent = sceneParent;
            var toScale = buildingController.CacheTransform.localScale;
            buildingController.CacheTransform.localScale = new Vector3(.1f, .1f, .1f);
            gameObject.SetActive(false);
            await buildingController.CacheTransform.DOScale(toScale, .75f).AsyncWaitForCompletion();
            
            LevelController.Instance.AddBuilding(buildingController);
        }

        private void OnDropCurrency(int instanceId, CurrencyType type)
        {
            if (instanceId != InstanceId) return;
            
            if (type == CurrencyType.Crystal)
            {
                _crrRequireCrystal--;
            }

            if (type == CurrencyType.Ruby)
            {
                _crrRequireRuby--;
            }
            
            UpdateText();
            
            if (_crrRequireCrystal <= 0 && _crrRequireRuby <= 0)
            {
                ConditionsAreMatch().Forget();
            }
        }

        private void UpdateText()
        {
            string text = String.Empty;
            if (_crrRequireCrystal > 0)
            {
                text += $"{_crrRequireCrystal} <sprite=\"{Constant.CrystalSpriteAsset}\" index=0>\n";
            }

            if (_crrRequireRuby > 0)
            {
                text += $"{_crrRequireRuby} <sprite=\"{Constant.RubySpriteAsset}\" index=0>";
            }
            _requirementPopup.UpdateText(text);
        }

        public void OnSensorPlayerDetected(GameObject obj, Sensor sensor)
        {
            Sequence t = DOTween.Sequence(_requirementPopup.CacheTransform);
            t.Join(_requirementPopup.CacheTransform.DOScale(Vector3.one * 1.25f, .5f));
            t.PrependInterval(.5f);
            t.Play();
        }

        public void OnSensorPlayerLostDetected(GameObject obj, Sensor sensor)
        {
            Sequence t = DOTween.Sequence(_requirementPopup.CacheTransform);
            t.Join(_requirementPopup.CacheTransform.DOScale(Vector3.one, .5f));
            t.PrependInterval(.25f);
            t.Play();
        }
    }
}

