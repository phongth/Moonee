using System.Collections;
using System.Collections.Generic;
using SensorToolkit;
using UnityEngine;

namespace Game
{
    public class Headquarter : BuildingController
    {
        protected override void OnDropCurrency(int instanceId, CurrencyType type)
        {
            if (instanceId != InstanceId) return;
        }
        public override void OnSensorPlayerDetected(GameObject obj, Sensor sensor)
        {
            
        }

        public override void OnSensorPlayerLostDetected(GameObject obj, Sensor sensor)
        {
            
        }
    }
}

