using System;
using Base;
using Base.MessageSystem;
using DG.Tweening;
using NaughtyAttributes;
using UnityEngine;

namespace Game
{
    public class BuildingController : BaseMono
    {
        [Header("Building Properties")] [SerializeField]
        private BuildingType buildingType;
        [SerializeField] private BuildingProperties properties;

        [SerializeField, ReadOnly] private Attribute attribute;

        [Header("Building UI")]
        // [SerializeField] private RequirementPopup prefabPopup;
        // [SerializeField] protected Transform offsetTransform;
        [SerializeField] private HealthBarManage prefabHealthBar;
        [SerializeField] protected Transform offsetHealthBar;
        
        public BuildingType BuildingType => buildingType;
        public Attribute PropertyAttribute => attribute;
        public Attribute DefaultAttribute => properties.Attribute;

        [SerializeField] protected InputCurrencyUi requirementPopup;
        protected HealthBarManage _healthBarManage;
        
        public Transform SceneParent { get; set; }

        protected virtual void Awake()
        {
            var mainCanvas = GameObject.FindGameObjectWithTag("Main Canvas").transform;
            
            //if (prefabPopup) _requirementPopup = Instantiate(prefabPopup, mainCanvas);
            if (prefabHealthBar) _healthBarManage = Instantiate(prefabHealthBar, mainCanvas);
            
            attribute = new Attribute(properties.Attribute);

            attribute.OnUpgrade += OnUpgradeCallback;
        }

        protected virtual void OnEnable()
        {
            // if (_requirementPopup)
            // {
            //     _requirementPopup.SetTarget(offsetTransform, Vector3.zero);
            //     _requirementPopup.Active = true;
            // }

            Canvas[] worldCanvas = GetComponentsInChildren<Canvas>();
            for (int i = 0; i < worldCanvas.Length; ++i)
            {
                
            }

            _healthBarManage.SetTarget(offsetHealthBar, Vector3.zero);
            _healthBarManage.Active = GameManager.GameLevel >= 5;
        }

        protected virtual void Start()
        {
            Messenger.RegisterListener<int, CurrencyType>(GameMessage.DropCurrencyBuilding, OnDropCurrency);
        }

        protected virtual void OnDisable()
        {
            if (requirementPopup) requirementPopup.Active = false;
            if (_healthBarManage) _healthBarManage.Active = false;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            
            Messenger.RemoveListener<int, CurrencyType>(GameMessage.DropCurrencyBuilding, OnDropCurrency);

            attribute.OnUpgrade -= OnUpgradeCallback;
            
            // if (requirementPopup != null)
            // {
            //     Destroy(requirementPopup.gameObject);
            // }
            
            if (_healthBarManage != null) Destroy(_healthBarManage.gameObject);
            
            //_requirementPopup = null;
            _healthBarManage = null;
        }

        protected void UpdateText()
        {
            string text = String.Empty;

            int originalCrystal = (int) DefaultAttribute.amountInCrystal.StatValue;
            int originalRuby = (int) DefaultAttribute.amountInRuby.StatValue;
            
            int numCrystal = originalCrystal - (int)PropertyAttribute.amountInCrystal.StatValue;
            int numRuby = originalRuby - (int)PropertyAttribute.amountInRuby.StatValue;

            numCrystal = Mathf.Clamp(numCrystal, 0, originalCrystal);
            numRuby = Mathf.Clamp(numRuby, 0, originalRuby);
            
            if (originalCrystal > 0)
            {
                text += $"{numCrystal}/{originalCrystal} <sprite=\"{Constant.CrystalSpriteAsset}\" index=0>\n";
            }

            if (originalRuby > 0)
            {
                text += $"{numRuby}/{originalRuby} <sprite=\"{Constant.RubySpriteAsset}\" index=0>";
            }

            //requirementPopup.Active = (numCrystal > 0 || numRuby > 0);

            requirementPopup.UpdateText(text);
        }

        protected virtual void OnDropCurrency(int instanceId, CurrencyType type) {}

        public virtual void LevelUp()
        {
            attribute.Upgrade();
            DefaultAttribute.Upgrade();
        }

        public virtual void OnUpgradeCallback()
        {
            
        }

        public virtual void OnSensorPlayerDetected(GameObject obj, SensorToolkit.Sensor sensor)
        {
            // Sequence t = DOTween.Sequence(_requirementPopup.CacheTransform);
            // t.Append(_requirementPopup.CacheTransform.DOScale(Vector3.one * 1.5f, .5f));
            // t.PrependInterval(.5f);
            // t.Play();
        }

        public virtual void OnSensorPlayerLostDetected(GameObject obj, SensorToolkit.Sensor sensor)
        {
            // Sequence t = DOTween.Sequence(_requirementPopup.CacheTransform);
            // t.Append(_requirementPopup.CacheTransform.DOScale(Vector3.one, .5f));
            // t.PrependInterval(.25f);
            // t.Play();
        }

        public virtual void HealthImpact(float amount)
        {
            PropertyAttribute.buildHealthPoint.StatValue = Mathf.Clamp(PropertyAttribute.buildHealthPoint.StatValue - amount, 0,
                properties.Attribute.buildHealthPoint.StatValue);

            float value = PropertyAttribute.buildHealthPoint.StatValue / DefaultAttribute.buildHealthPoint.StatValue;
            value = Mathf.Clamp01(value);
            _healthBarManage.Fill(value);

            if (PropertyAttribute.buildHealthPoint.StatValue <= 0)
            {
                // Building destroyed
                LevelController.Instance.RemoveBuilding(this);
                Destroy(CacheGameObject);
            }
        }
    }
}

