using System;
using System.Collections;
using System.Collections.Generic;
using Base;
using Base.Pattern;
using Cysharp.Threading.Tasks;
using SensorToolkit;
using UnityEngine;

namespace Game
{
    public class CrystalMineController : BaseMono
    {
        public int crystalCapacity = 0;
        public float harvestDelay = .5f;

        private bool _timeRun = false;
        private float _crrTime = 0;
        private int _crrCapacity = 0;

        private Coroutine _harvestCoroutine;

        private void OnEnable()
        {
            _crrTime = harvestDelay;
            _crrCapacity = crystalCapacity;
        }

        public void StartMiningCrystal(PlayerController player)
        {
            _timeRun = true;
            if (_harvestCoroutine == null)
            {
                _harvestCoroutine = StartCoroutine(Harvest(player));
            }
        }

        public void StopMiningCrystal(PlayerController player)
        {
            if (_harvestCoroutine != null)
            {
                _timeRun = false;
                StopCoroutine(Harvest(player));
                _harvestCoroutine = null;
            }
            
        }

        private IEnumerator Harvest(PlayerController player)
        {
            while (_timeRun)
            {
                if (_crrCapacity <= 0)
                {
                    OutOfCrystal();
                    yield break;
                }

                _crrTime -= Time.deltaTime;

                if (_crrTime <= 0)
                {
                    OutputCrystal(player);
                    _crrTime = harvestDelay;
                }

                yield return null;
            }

            yield return null;
        }

        private void OutputCrystal(PlayerController player)
        {
            Transform crystal = ObjectPooler.Get("Crystal", Position, Vector3.zero);
            var currency = crystal.GetComponent<CurrencyItem>();
            _crrCapacity--;
            player.HarvestCrystal(currency).Forget();
        }

        private void OutOfCrystal()
        {
            GetComponent<ObjectSpawnCs>().OnInteract();
        }
    }
}

