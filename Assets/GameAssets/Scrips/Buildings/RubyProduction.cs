using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Base;
using Base.Pattern;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using TMPro;
using UnityEngine;

namespace Game
{
    public sealed class RubyProduction : BuildingController
    {
        [Header("Scene reference")]
        [SerializeField] private Transform sceneParent;

        [Header("Output Grid Setting")]
        [SerializeField] private int width;
        [SerializeField] private int height;
        [SerializeField] private float padding;
        [SerializeField] private Transform gridRoot;

        private GridBase _gridBase = new GridBase(0, 0, 0);
        private float _timeElapsed;
        
        /// <summary>
        /// Condition of ruby production:
        /// </summary>
        /// <list type="Conditions">
        /// <item><see cref="Conditions.IsTimeRun"/> The time condition </item>
        /// <item><see cref="Conditions.IsFullUp"/> Full-up condition: if the grid is full of ruby, return true </item>
        /// </list>
        private Conditions _condition = new Conditions(false, false);
        private Queue<int> _produceQueue = new Queue<int>();
        private int _crrGridIndex = 0;

        protected override void Start()
        {
            base.Start();

            _gridBase = new GridBase(width, height, padding);

            for (int i = 0; i < _gridBase.GridList.Capacity; ++i)
            {
                _gridBase.Add(0);
            }

            ObjectPooler.InitObjectPool("Ruby");

            UpdateText();
        }

        private void Update()
        {
            if (_condition.IsTimeRun && !_condition.IsFullUp)
            {
                _timeElapsed -= Time.deltaTime;

                if (_timeElapsed <= 0)
                {
                    _timeElapsed = PropertyAttribute.productionSpeed.StatValue;
                    OutputRuby().Forget();
                }
            }
        }

        private void OnDrawGizmos()
        {
            if (_gridBase != null && !Application.isPlaying)
            {
                _gridBase = new GridBase(width, height, padding);
                
                for (int x = 0; x < height; ++x)
                {
                    for (int y = 0; y < width; ++y)
                    {
                        Vector3 position = _gridBase.GetWorldPosition(x, y);
                        Gizmos.DrawIcon(gridRoot.TransformPoint(new Vector3(position.x, 0, position.y)),
                            "BaseObjectSpawner/spawner_icon.png", true);
                    }
                }
            }
        }

        private async UniTask OutputRuby()
        {
            if (_produceQueue.Count > 0)
            {
                int index = _produceQueue.Dequeue();
                
                PropertyAttribute.amountInCrystal.StatValue++;
                PropertyAttribute.amountInCrystal.StatValue = Mathf.Clamp(PropertyAttribute.amountInCrystal.StatValue, 0,
                    DefaultAttribute.amountInCrystal.StatValue * PropertyAttribute.buildingLevel.StatValue);
                
                var ruby = ObjectPooler.Get("Ruby", Vector3.zero, Vector3.zero, sceneParent);
                ruby.localScale = Vector3.one;

                CurrencyItem rubyCurrency = ruby.GetComponent<CurrencyItem>();
                rubyCurrency.OnInteract += OnRubyInteract;
                
                Sequence t = DOTween.Sequence(ruby);
            
                Vector2 pos = _gridBase.Get(index);
                Vector3 pos2 = _gridBase.GetWorldPosition((int)pos.y, (int)pos.x);
                Vector3 position = gridRoot.TransformPoint(new Vector3(pos2.x, 0, pos2.y));

                int indexGrid = _gridBase.GridList.FindIndex(item => item == 1);
                _gridBase.SetValue(indexGrid, rubyCurrency.InstanceId);

                t.Append(ruby.DOMove(position, .75f).SetEase(Ease.OutSine).OnComplete(() =>
                {
                    _condition.IsTimeRun = true;
                    _timeElapsed = PropertyAttribute.productionSpeed.StatValue;
                }));

                await t.Play().AsyncWaitForCompletion();

                int countRubyProduce = _gridBase.GridList.FindAll(item => item != 0 && item != 1).Count;
                if (countRubyProduce >= PropertyAttribute.amountOut.StatValue)
                {
                    _condition.IsFullUp = true;
                    _timeElapsed = PropertyAttribute.productionSpeed.StatValue;
                }
                
                UpdateText();
            }
            else
            {
                _condition.IsTimeRun = false;
            }
        }

        private void OnRubyInteract(CurrencyItem ruby)
        {
            ruby.OnInteract -= OnRubyInteract;

            int index = _gridBase.GetIndex(ruby.InstanceId);
            _gridBase.SetValue(index, 0);
            _condition.IsFullUp = false;

            // PropertyAttribute.amountInCrystal.StatValue++;
            // PropertyAttribute.amountInCrystal.StatValue = Mathf.Clamp(PropertyAttribute.amountInCrystal.StatValue, 0,
            //     DefaultAttribute.amountInCrystal.StatValue * PropertyAttribute.buildingLevel.StatValue);
            //
            // UpdateText();
        }

        protected override void OnDropCurrency(int instanceId, CurrencyType type)
        {
            if (instanceId != InstanceId || PropertyAttribute.amountInCrystal.StatValue <= 0) return;

            PropertyAttribute.amountInCrystal.StatValue -= 1;
            _crrGridIndex = _gridBase.GridList.FindIndex(item => item == 0);
            _gridBase.SetValue(_crrGridIndex, 1);
            _produceQueue.Enqueue(_crrGridIndex);

            if (_produceQueue.Count > 0)
            {
                _condition.IsTimeRun = true;
                if (_timeElapsed <= 0)
                {
                    _timeElapsed = PropertyAttribute.productionSpeed.StatValue;
                }
            }

            UpdateText();
        }

        public override void OnUpgradeCallback()
        {
            UpdateText();
        }
    }

    public class Conditions
    {
        public bool IsTimeRun;
        public bool IsFullUp;
        public Conditions(bool item1, bool item2)
        {
            IsTimeRun = item1;
            IsFullUp = item2;
        }
    }
}

