using System.Collections.Generic;
using Base.MessageSystem;
using Base.Module;
using TMPro;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

namespace Game
{
    public class Laboratory : BuildingController
    {
        [System.Serializable]
        internal class LabUnlockItem
        {
            public LockType lockType;
            public Sprite sprite;
            public int requireCrystal;
            public int requireRuby;
        }

        [Header("Laboratory")] 
        [SerializeField] private float unlockInterval;
        [SerializeField, CustomClassDrawer] private List<LabUnlockItem> unlockList = new List<LabUnlockItem>();
        [SerializeField] private Image image;

        private Queue<LabUnlockItem> _unlockQueue = new Queue<LabUnlockItem>();
        private float _timeElapsed;
        private LabUnlockItem _crrItem;
        private bool _isTimeRun = false;

        protected override void Start()
        {
            base.Start();

            for (int i = 0; i < unlockList.Count; ++i)
            {
                _unlockQueue.Enqueue(unlockList[i]);
            }

            _timeElapsed = unlockInterval;
            requirementPopup.Active = false;
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            _isTimeRun = true;
            
        }

        private void Update()
        {
            if (_isTimeRun)
            {
                _timeElapsed -= Time.deltaTime;

                if (_timeElapsed <= 0)
                {
                    UnlockItem();
                    _timeElapsed = unlockInterval;
                }
            }
        }

        private void UnlockItem()
        {
            LabUnlockItem item = _unlockQueue.Dequeue();

            _crrItem = new LabUnlockItem
            {
                lockType = item.lockType, requireCrystal = item.requireCrystal, requireRuby = item.requireRuby,
                sprite = item.sprite
            };
            
            PropertyAttribute.amountInCrystal.StatValue = _crrItem.requireCrystal;
            PropertyAttribute.amountInRuby.StatValue = _crrItem.requireRuby;

            DefaultAttribute.amountInCrystal.StatValue = _crrItem.requireCrystal;
            DefaultAttribute.amountInRuby.StatValue = _crrItem.requireRuby;

            image.sprite = _crrItem.sprite;
            
            UpdateText();
            
            _unlockQueue.Enqueue(item);

            requirementPopup.Active = true;
            _isTimeRun = false;
        }

        protected override void OnDropCurrency(int instanceId, CurrencyType type)
        {
            if (InstanceId != instanceId) return;

            if (type == CurrencyType.Crystal)
            {
                PropertyAttribute.amountInCrystal.StatValue--;
            }

            if (type == CurrencyType.Ruby)
            {
                PropertyAttribute.amountInRuby.StatValue--;
            }

            UpdateText();
            
            if (PropertyAttribute.amountInCrystal.StatValue <= 0 && PropertyAttribute.amountInRuby.StatValue <= 0)
            {
                Messenger.RaiseMessage(GameMessage.UnlockUpgrade, _crrItem.lockType);
                _isTimeRun = true;
                requirementPopup.Active = false;
            }
        }
    }
}

