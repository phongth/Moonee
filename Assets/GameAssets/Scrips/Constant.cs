
namespace Game
{
    public enum NpcType {Ally, Enemy}
    
    public enum GameMessage {DropCurrencyBuilding, DropCurrencyConstruction, UnlockUpgrade, DropCurrencyUpgrade, UpdateResourceUi, OnLevelCompleted, EnemyEliminated}
    
    public enum BuildingType {Headquarter = 0, RubyProduction, TroopBarrack, Laboratory}

    public enum UpgradeType {Character, Building}

    public enum CurrencyType
    {
        Crystal = 0,
        Ruby,
        None,
        Both
    }

    public enum OutputType
    {
        Currency, Troops, None
    }
    
    public enum CharacterType {Player, Npc}
    
    public struct Constant
    {
        public const string CrystalSpriteAsset = "crystal_1";
        public const string RubySpriteAsset = "ruby";

        public const string CrystalSpriteAssetUI = "crystal_2";
        public const string RubySpriteAssetUI = "ruby_1";
    }

    [System.Serializable]
    public class SaveData
    {
        public int gameLevel;
        public bool isVibrate;
    }

    public static class Setting
    {
        public static bool isVibrate = true;
    }

}


