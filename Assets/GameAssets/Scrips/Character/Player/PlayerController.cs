using System;
using Base;
using Base.MessageSystem;
using Base.Module;
using Base.Pattern;
using UnityEngine;
using DG.Tweening;
using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using NaughtyAttributes;
using SensorToolkit;

namespace Game
{
    public class PlayerController : BaseMono
    {
        [SerializeField] private CharacterProperty defaultProperty;
        [SerializeField] private Transform currencyParent;
        private Rigidbody _body;
        private int _index = 0;
        private bool _isDropAll = false;

        private readonly Queue<CurrencyItem> _crystalList = new Queue<CurrencyItem>();
        private Queue<CurrencyItem> _rubyList = new Queue<CurrencyItem>();
        [SerializeField, ReadOnly] private List<CrystalMineController> _mineList = new List<CrystalMineController>();

        [SerializeField, ReadOnly] private CharacterAttribute _crrAttribute;

        private void Start()
        {
            _body = GetComponent<Rigidbody>();

            ResetDefault();
        }

        private void OnTriggerEnter(Collider other)
        {
            CurrencyItem currency = other.GetComponent<CurrencyItem>();
            if (currency && currencyParent.childCount < _crrAttribute.loadCapacity.StatValue)
            {
                if (currency.type == CurrencyType.Crystal && !currency.Ignore)
                {
                    // currency.GetComponent<ObjectSpawnCs>().OnInteract();
                    // //other.GetComponent<CurrencyItem>().Ignore = true;
                    // //Destroy(other.gameObject);
                    // var crystal = ObjectPooler.Get("Crystal", currency.Position, currency.EulerAngles);
                    // var crystalCurrency = crystal.GetComponent<CurrencyItem>();
                    // crystalCurrency.Ignore = true;
                    // _crystalList.Enqueue(crystalCurrency);
                    // PickUpCurrency(crystal).Forget();
                    // //other.GetComponent<CurrencyItem>().Ignore = true;
                }
                else if (currency.type == CurrencyType.Ruby && !currency.Ignore)
                {
                    currency.Interact();
                    currency.Ignore = true;
                    _rubyList.Enqueue(currency);
                    DOTween.Kill(currency.CacheTransform);
                    PickUpCurrency(currency.CacheTransform).Forget();
                }
            }
        }

        private void OnTriggerStay(Collider other)
        {
            if (_body.velocity.magnitude >= .5f) return;

            if (other.CompareTag("Construction") && !_isDropAll)
            {
                _isDropAll = true;

                var construction = other.GetComponentInBranch<ConstructionSpot>();
                int numOfCrystal = _crystalList.Count;
                int numOfRuby = _rubyList.Count;
                int requireCrystal = construction.RequireCrystal;
                if (requireCrystal > 0)
                {
                    int amountDrop = numOfCrystal >= requireCrystal ? requireCrystal : numOfCrystal;

                    DropCurrency(other.transform, amountDrop, CurrencyType.Crystal, () =>
                        {
                            var constructionSpot = other.GetComponentInBranch<ConstructionSpot>();
                            Messenger.RaiseMessage(GameMessage.DropCurrencyConstruction, constructionSpot.InstanceId, CurrencyType.Crystal);
                        }).Forget();
                }

                int requireRuby = construction.RequireRuby;
                if (requireRuby > 0)
                {
                    int amountDrop = numOfRuby >= requireRuby ? requireRuby : numOfRuby;

                    DropCurrency(other.transform, amountDrop, CurrencyType.Ruby, () =>
                        {
                            var constructionSpot = other.GetComponentInBranch<ConstructionSpot>();
                            Messenger.RaiseMessage(GameMessage.DropCurrencyConstruction, constructionSpot.InstanceId, CurrencyType.Ruby);
                        })
                        .Forget();
                }
            }
            else if (other.CompareTag("Building Input"))
            {
                if (_isDropAll) return;
                _isDropAll = true;
                var building = other.GetComponentInBranch<BuildingController>();
                DropOnBuilding(building).Forget();
            }
            else if (other.CompareTag("Building Upgrade") && !_isDropAll)
            {
                _isDropAll = true;
                var buildingUnlockUpgrade = other.GetComponentInBranch<BuildingController, UnlockUpgrade>();
                UpgradeBuilding(buildingUnlockUpgrade).Forget();
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if ((other.CompareTag("Construction") || other.CompareTag("Building Input") || other.CompareTag("Building Upgrade")) && _isDropAll)
            {
                _isDropAll = false;
            }
        }

        private async UniTask PickUpCurrency(Transform currencyTransform)
        {
            currencyTransform.SetParent(currencyParent);
            Sequence t = DOTween.Sequence();
            var index = _index / 5;
            t.Append(currencyTransform.DOLocalMove(new Vector3(0, (_index % 5) * .275f, 0.525f * index), .25f).SetEase(Ease.InSine));
            t.Join(currencyTransform.DOLocalRotate(Vector3.zero, .25f));
            _index++;
            await t.Play().AsyncWaitForCompletion();
            
            Messenger.RaiseMessage(GameMessage.UpdateResourceUi, _crystalList.Count, _rubyList.Count);
        }

        public async UniTask HarvestCrystal(CurrencyItem crystal)
        {
            if (currencyParent.childCount < _crrAttribute.loadCapacity.StatValue)
            {
                crystal.Ignore = true;
                _crystalList.Enqueue(crystal);
                await PickUpCurrency(crystal.CacheTransform);
            }
            else
            {
                foreach (var mine in _mineList)
                {
                    mine.StopMiningCrystal(this);
                    crystal.Ignore = false;
                    ObjectPooler.Return("Crystal", crystal.CacheTransform);
                }
            }
        }

        private async UniTask DropCurrency(Transform destination, int amount, CurrencyType type, Action callback = null)
        {
            Vector3 pos = destination.position;
            Sequence t = DOTween.Sequence();

            if (type == CurrencyType.Crystal)
            {
                var childCount = _crystalList.Count;
                int length = amount >= childCount ? childCount : amount;

                for (int i = 0; i < length; ++i)
                {
                    var crystal = _crystalList.Dequeue();
                    var child = crystal.CacheTransform;
                    currencyParent.RemoveChildren(child);
                    t.Append(child.DOMove(pos, .1f)
                        .SetEase(Ease.OutCubic));
                    t.AppendCallback(() =>
                    {
                        var crystalCurrency = crystal;
                        ObjectPooler.Return("Crystal", crystalCurrency.CacheTransform);
                        crystalCurrency.Ignore = false;
                        callback?.Invoke();
                    });
                    t.AppendInterval(.01f);
                }
            }
            else if (type == CurrencyType.Ruby)
            {
                var rubyCount = _rubyList.Count;
                int length = amount >= rubyCount ? rubyCount : amount;

                for (int i = 0; i < length; ++i)
                {
                    var ruby = _rubyList.Dequeue();
                    var child = ruby.CacheTransform;
                    currencyParent.RemoveChildren(child);
                    t.Append(child.DOMove(pos, .1f)
                        .SetEase(Ease.OutCubic));
                    t.AppendCallback(() =>
                    {
                        var rubyCurrency = ruby;
                        ObjectPooler.Return("Ruby", rubyCurrency.CacheTransform);
                        rubyCurrency.Ignore = false;
                        callback?.Invoke();
                    });
                    t.AppendInterval(.01f);
                }
            }

            //t.AppendCallback(() => callback?.Invoke());
            t.Play();
            await t.AsyncWaitForCompletion();
            ReArrangeCurrency().Forget();
            Messenger.RaiseMessage(GameMessage.UpdateResourceUi, _crystalList.Count, _rubyList.Count);
        }

        private async UniTask ReArrangeCurrency()
        {
            int currencyCount = currencyParent.childCount;
            if (currencyCount > 0)
            {
                Sequence t = DOTween.Sequence();
                _index = 0;
                for (int i = 0; i < currencyCount; ++i)
                {
                    int index = _index / 5;
                    var currency = currencyParent.GetChild(i);
                    t.Append(currency.DOLocalMove(new Vector3(0, (_index % 5) * .275f, 0.525f * index), .1f).SetEase(Ease.InSine));
                    _index++;
                }

                t.Play();
                await t.AsyncWaitForCompletion();
            }
            else
            {
                _index = 0;
                await UniTask.Yield();
            }
        }

        private async UniTask DropOnBuilding(BuildingController building)
        {
            int numOfCrystal = _crystalList.Count;
            int numOfRuby = _rubyList.Count;

            int requireCrystal = (int)building.PropertyAttribute.amountInCrystal.StatValue;
            int requireRuby = (int)building.PropertyAttribute.amountInRuby.StatValue;

            if (requireCrystal > 0)
            {
                int amount = numOfCrystal >= requireCrystal ? requireCrystal : numOfCrystal;

                await DropCurrency(building.transform, amount, CurrencyType.Crystal, () =>
                {
                    var _building = building;
                    Messenger.RaiseMessage(GameMessage.DropCurrencyBuilding, _building.InstanceId, CurrencyType.Crystal);
                });
            }

            if (requireRuby > 0)
            {
                int amount = numOfRuby >= requireRuby ? requireRuby : numOfRuby;

                await DropCurrency(building.transform, amount, CurrencyType.Ruby, () =>
                {
                    var _building = building;
                    Messenger.RaiseMessage(GameMessage.DropCurrencyBuilding, _building.InstanceId, CurrencyType.Ruby);
                });
            }

            if (requireCrystal <= -1)
            {
                int amount = numOfCrystal;

                await DropCurrency(building.transform, amount, CurrencyType.Crystal, () =>
                {
                    var _building = building;
                    Messenger.RaiseMessage(GameMessage.DropCurrencyBuilding, _building.InstanceId, CurrencyType.Crystal);
                });
            }

            if (requireRuby <= -1)
            {
                int amount = numOfRuby;

                await DropCurrency(building.transform, amount, CurrencyType.Ruby, () =>
                {
                    var _building = building;
                    Messenger.RaiseMessage(GameMessage.DropCurrencyBuilding, _building.InstanceId, CurrencyType.Ruby);
                });
            }
        }

        private async UniTask UpgradeBuilding(UnlockUpgrade unlock)
        {
            
            int requireCrystal = unlock.requireCrystal;
            int requireRuby = unlock.requireRuby;

            int numOfCrystal = _crystalList.Count;
            int numOfRuby = _rubyList.Count;

            if (requireCrystal > 0)
            {
                int amountDrop = numOfCrystal >= requireCrystal ? requireCrystal : numOfCrystal;

                await DropCurrency(unlock.transform, amountDrop, CurrencyType.Crystal, () =>
                {
                    var _building = unlock.GetComponentInBranch<BuildingController>();
                    Messenger.RaiseMessage(GameMessage.DropCurrencyUpgrade, _building.InstanceId, CurrencyType.Crystal);
                });
            }

            if (requireRuby > 0)
            {
                int amountDrop = numOfRuby >= requireRuby ? requireRuby : numOfRuby;

                await DropCurrency(unlock.transform, amountDrop, CurrencyType.Ruby, () =>
                {
                    var _building = unlock.GetComponentInBranch<BuildingController>();
                    Messenger.RaiseMessage(GameMessage.DropCurrencyUpgrade, _building.InstanceId, CurrencyType.Ruby);
                });
            }
        }

        public void OnCrystalMineDetected(GameObject obj, Sensor sensor)
        {
            if (currencyParent.childCount >= _crrAttribute.loadCapacity.StatValue) return;
            
            _mineList.AddRangeIfNotContains(sensor.GetDetectedByComponent<CrystalMineController>());

            var nearest = _mineList[0];
            var crystalMine = nearest;
            
            for (int i = 1; i < _mineList.Count; ++i)
            {
                _mineList[i].StopMiningCrystal(this);
            }
            
            crystalMine.StartMiningCrystal(this);
        }

        public void OnCrystalMineLostDetection(GameObject obj, Sensor sensor)
        {
            var mineController = obj.GetComponent<CrystalMineController>();
            mineController.StopMiningCrystal(this);
            _mineList.Remove(mineController);
        }

        public void CharacterMove(Vector3 motion)
        {
            Vector3 rigidVelocity = _body.velocity;
            _body.velocity = new Vector3(motion.x, rigidVelocity.y, motion.z);
        }

        public void CharacterRotate(Quaternion motion)
        {
            _body.rotation = motion;
        }

        public void LevelUp()
        {
            _crrAttribute.Upgrade();
        }

        public void ResetDefault()
        {
            _crrAttribute ??= new CharacterAttribute();

            _crrAttribute.Init(defaultProperty.Attribute);
        }
    }
}
