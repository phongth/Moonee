using System.Collections;
using Base.Pattern;
using UnityEngine;

namespace Game
{
    public class NpcMoveState : CharacterState
    {
        [SerializeField, Range(25f, 100f)] private float speed;
        private NpcController _npcController;
        private SoldierAI _soldierAI;

        private bool _switchToAttack = false;
        private bool _switchToIdle = false;
        private Rigidbody _body;

        private Vector3 _move;
        private Vector3 _face;

        private float _timer = 1f;

        protected override void Start()
        {
            base.Start();

            _npcController = CharacterStateController.GetComponent<NpcController>();
            _soldierAI = CharacterStateController.GetComponent<SoldierAI>();
            _body = CharacterStateController.GetComponent<Rigidbody>();
        }

        public override void EnterState(float dt, CharacterState fromState)
        {
            
        }

        public override void FixedUpdateBehaviour(float dt)
        {
           HandleMovement(dt);
        }

        public override void UpdateBehaviour(float dt)
        {
            if (_timer >= 0f)
            {
                _timer -= dt;
            }
            else
            {
                _timer = 1f;
                StartCoroutine(CheckingEnemy());
            }
        }

        public override void PostUpdateBehaviour(float dt)
        {
            if (CharacterStateController.Animator == null) return;
            if (CharacterStateController.Animator.runtimeAnimatorController == null) return;
            if (!CharacterStateController.Animator.enabled) return;
        }

        public override void CheckExitTransition()
        {
            if (_switchToIdle) CharacterStateController.EnqueueTransition<NpcIdleState>();
            else if (_switchToAttack) CharacterStateController.EnqueueTransition<NpcAttackState>();
        }

        public override void ExitStateBehaviour(float dt, CharacterState toState)
        {
            _switchToAttack = false;
            _switchToIdle = false;
            StopCoroutine(CheckingEnemy());
            if (_npcController.NpcType == NpcType.Enemy) StopCoroutine(CheckingBuilding());
        }

        private void HandleMovement(float dt)
        {
            _body.velocity = _move.normalized * dt * speed;
            
            var a = Vector3.SignedAngle(_body.transform.forward, _face, Vector3.up);
            var torque = Mathf.Clamp(a / 10f, -1f, 1f) * 10f;
            //_body.AddTorque(Vector3.up * torque, ForceMode.Impulse);
            _body.angularVelocity = Vector3.up * torque * dt * speed;
        }

        private IEnumerator CheckingEnemy()
        {
            var enemySpot = _soldierAI.EnemiesSpotted;
            if (enemySpot.Count > 0)
            {
                var target = enemySpot[0];
                var cooldown = Random.Range(0.5f, 2f);
                if ((target.Position - _npcController.Position).magnitude > _npcController.StateParam.attackRange)
                {
                    while (cooldown > 0)
                    {
                        if (target == null) break;
                        
                        var targetDirection = (target.Position - _npcController.Position).normalized;
                        _move = _soldierAI.GetSteeredDirection(targetDirection);
                        _face = targetDirection;
                        
                        cooldown -= Time.deltaTime;
                        yield return null;
                    }
                }
                else
                {
                    _switchToAttack = true;
                }
            }
            else
            {
                if (_npcController.NpcType == NpcType.Ally)
                {
                    var cooldown = Random.Range(0.5f, 2f);
                    if (_npcController.TargetPosition != Vector3.zero)
                    {
                        if (Mathf.RoundToInt(Vector3.Distance(_npcController.Position, _npcController.TargetPosition)) == 0)
                        {
                            _npcController.SetTargetPosition(Vector3.zero);
                            _switchToIdle = true;
                            yield return null;
                        }
                        var targetDirection = (_npcController.TargetPosition - _npcController.Position).normalized;
                        _move = _soldierAI.GetSteeredDirection(targetDirection);
                        _face = targetDirection;
                    }
                }
                else
                {
                    StartCoroutine(CheckingBuilding());
                }
            }

            yield return null;
        }
        
        private IEnumerator CheckingBuilding()
        {
            var buildingSpot = _soldierAI.BuildingSpotted;
            if (buildingSpot.Count > 0)
            {
                var target = buildingSpot[0];
                float cooldown = Random.Range(.5f, 2f);
                if ((target.Position - _npcController.Position).magnitude > _npcController.StateParam.attackRange)
                {
                    while (cooldown > 0)
                    {
                        if (target == null) yield break;

                        var targetDirection = (target.Position - _npcController.Position).normalized;
                        _move = _soldierAI.GetSteeredDirection(targetDirection);
                        _face = targetDirection;

                        cooldown -= Time.deltaTime;
                        yield return null;
                    }
                }
                else _switchToAttack = true;
            }
            else _switchToIdle = true;

            yield return null;
        }
    }
}
