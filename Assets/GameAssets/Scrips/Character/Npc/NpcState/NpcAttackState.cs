using System.Collections;
using Base.Pattern;
using UnityEngine;

namespace Game
{
    public class NpcAttackState : CharacterState
    {
        [SerializeField] private float firingRate;
        
        
        private NpcController _npcController;
        private SoldierAI _soldierAI;
        private Rigidbody _body;

        private bool _lostEnemy = false;

        private Vector3 _move;
        private Vector3 _face;

        private float _timer = 1f;
        private float _shootingTimer;

        protected override void Start()
        {
            base.Start();

            _npcController = CharacterStateController.GetComponent<NpcController>();
            _soldierAI = CharacterStateController.GetComponent<SoldierAI>();
            _body = CharacterStateController.GetComponent<Rigidbody>();
        }

        public override void EnterState(float dt, CharacterState fromState)
        {
            _lostEnemy = false;
            _shootingTimer = firingRate;
            StartCoroutine(CheckingEnemy());
        }

        public override void FixedUpdateBehaviour(float dt)
        {
            HandleMovement(dt);
        }
        
        public override void UpdateBehaviour(float dt)
        {
            if (_timer >= 0f)
            {
                _timer -= dt;
            }
            else
            {
                _timer = 1f;
                StartCoroutine(CheckingEnemy());
            }
        }

        public override void CheckExitTransition()
        {
            if (_lostEnemy) CharacterStateController.EnqueueTransition<NpcMoveState>();
        }

        public override void ExitStateBehaviour(float dt, CharacterState toState)
        {
            base.ExitStateBehaviour(dt, toState);

            _lostEnemy = false;
            StopCoroutine(CheckingEnemy());
            if (_npcController.NpcType == NpcType.Enemy) StopCoroutine(CheckingBuilding());
        }

        private void HandleMovement(float dt)
        {
            _body.velocity = _move.normalized * dt * 75f;
            
            var a = Vector3.SignedAngle(_body.transform.forward, _face, Vector3.up);
            var torque = Mathf.Clamp(a / 10f, -1f, 1f) * 10f;
            //_body.AddTorque(Vector3.up * torque, ForceMode.Impulse);
            _body.angularVelocity = Vector3.up * torque * dt * 50f;
        }

        private IEnumerator CheckingEnemy()
        {
            var enemySpot = _soldierAI.EnemiesSpotted;
            
            if (enemySpot.Count > 0)
            {
                var target = enemySpot[0];
                var cooldown = Random.Range(0.5f, 2f);
                var strafeDirection = new Vector3(Random.Range(-1f, 1f), 0f, Random.Range(-1f, 1f)).normalized;
                var direction = target.Position - _npcController.Position;
                if (direction.magnitude >= _npcController.StateParam.attackRange / 4f && direction.magnitude <= _npcController.StateParam.attackRange)
                {
                    while (cooldown > 0)
                    {
                        if (target == null) break;
                        
                        var targetDirection = (target.Position - _npcController.Position).normalized;
                        _move = _soldierAI.GetSteeredDirection(strafeDirection);
                        _face = targetDirection;
                        Fire(targetDirection);
                        cooldown -= Time.deltaTime;
                        _shootingTimer -= Time.deltaTime;
                        yield return null;
                    }
                }
                else
                { 
                    _lostEnemy = true;
                }
            }
            else
            {
                if (_npcController.NpcType == NpcType.Enemy) StartCoroutine(CheckingBuilding());
                else _lostEnemy = true;
            }

            yield return null;
        }

        private IEnumerator CheckingBuilding()
        {
            var buildingSpot = _soldierAI.BuildingSpotted;

            if (buildingSpot.Count > 0)
            {
                var target = buildingSpot[0];
                var cooldown = Random.Range(.5f, 2f);
                var strafeDirection = new Vector3(Random.Range(-1f, 1f), 0f, Random.Range(-1f, 1f)).normalized;
                var direction = target.Position - _npcController.Position;
                if (direction.magnitude >= _npcController.StateParam.attackRange / 4f && direction.magnitude <= _npcController.StateParam.attackRange)
                {
                    while (cooldown > 0)
                    {
                        if (target == null) break;
                        
                        var targetDirection = (target.Position - _npcController.Position).normalized;
                        _move = _soldierAI.GetSteeredDirection(strafeDirection);
                        _face = targetDirection;
                        Fire(targetDirection);
                        cooldown -= Time.deltaTime;
                        _shootingTimer -= Time.deltaTime;
                        yield return null;
                    }
                }
                else
                {
                    _lostEnemy = true;
                }
            }
            else
            {
                _lostEnemy = true;
            }

            yield return null;
        }

        private void Fire(Vector3 direction)
        {
            if (_shootingTimer <= 0)
            {
                _npcController.Shoot(direction);
                _shootingTimer = firingRate;
            }
        }
    }
}

