using Base.Module;
using Base.Pattern;
using UnityEngine;

namespace Game
{
    public class NpcIdleState : CharacterState
    {
        [SerializeField] private float intervalChecking = 1f;

        private float _timer;
        private SoldierAI _soldier;
        private NpcController _controller;
        private Rigidbody _body;

        protected override void Awake()
        {
            base.Awake();

            _soldier = CharacterStateController.GetComponent<SoldierAI>();
            _controller = CharacterStateController.GetComponent<NpcController>();
            _body = CharacterStateController.GetComponent<Rigidbody>();
        }

        public override void EnterState(float dt, CharacterState fromState)
        {
            base.EnterState(dt, fromState);

            _timer = intervalChecking;

            _body.velocity = Vector3.zero;
            _body.angularVelocity = Vector3.zero;
            
            _body.rotation = Quaternion.identity;
        }

        public override void FixedUpdateBehaviour(float dt)
        {
            
        }

        public override void CheckExitTransition()
        {
            if (_timer > 0)
            {
                _timer -= Time.fixedDeltaTime;
            }
            else
            {
                if (_soldier.EnemiesSpotted.Count > 0 || _controller.TargetPosition != Vector3.zero || _soldier.BuildingSpotted.Count > 0) 
                    CharacterStateController.EnqueueTransition<NpcMoveState>();
                else _timer = intervalChecking;
            }
        }
    }
}

