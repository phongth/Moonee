using System;
using Base;
using Base.Module;
using Base.Pattern;
using NaughtyAttributes;
using SensorToolkit;
using UnityEngine;

namespace Game
{
    public class NpcController : BaseMono
    {
        [SerializeField] protected NpcType npcType;
        [SerializeField] protected NpcStateParam stateParam;
        [SerializeField] protected Transform shootingPoint;
        [SerializeField] protected CharacterProperty defaultProperty;

        protected CharacterAttribute _attribute;

        protected Vector3 targetPosition;
        public Vector3 TargetPosition => targetPosition;
        
        public Transform ShootingPoint => shootingPoint;

        public NpcType NpcType => npcType;
        public NpcStateParam StateParam => stateParam;

        protected virtual void Start()
        {
            
        }

        private void OnEnable()
        {
            _attribute = new CharacterAttribute();
            _attribute.Init(defaultProperty.Attribute);
        }

        public void Shoot(Vector3 direction)
        {
            var bullet = ObjectPooler.Get("Projectile", ShootingPoint.position, Vector3.zero);
            var c = bullet.GetComponent<ProjectileController>();
            c.Direction = direction;
            c.Damage = _attribute.attackDamage.StatValue;
            c.Source = this;
            var sensor = bullet.GetComponent<RaySensor>();
            sensor.IgnoreList.Clear();
            sensor.IgnoreList.Add(CacheGameObject);
        }

        public void SetTargetPosition(Vector3 position)
        {
            targetPosition = position;
        }

        public virtual void LevelUp()
        {
            _attribute.Upgrade();
        }

        public virtual void HealthImpact(float damage)
        {
            _attribute.healthPoint.StatValue = Mathf.Clamp(_attribute.healthPoint.StatValue - damage, 0, 
                defaultProperty.Attribute.healthPoint.StatValue);
        }
    }
}

