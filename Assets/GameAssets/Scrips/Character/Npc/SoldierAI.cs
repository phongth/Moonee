using System;
using System.Collections;
using System.Collections.Generic;
using Base.Pattern;
using Cysharp.Threading.Tasks;
using SensorToolkit;
using UnityEngine;

namespace Game
{
    public class SoldierAI : MonoBehaviour
    {
        [SerializeField] private TriggerSensor sight;
        [SerializeField] private SteeringRig steerSensor;
        [SerializeField] private TriggerSensor interactionRange;

        private NpcController _npcController;
        private NpcStateParam StateParam => _npcController.StateParam;

        public List<NpcController> EnemiesSpotted
        {
            get
            {
                var detected = sight.GetDetectedByComponent<NpcController>();
                return detected.FindAll(item => item.NpcType != _npcController.NpcType);
            }
        }

        public List<BuildingController> BuildingSpotted
        {
            get
            {
                var detected = sight.GetDetectedByComponent<BuildingController>();
                
                if (_npcController.NpcType == NpcType.Ally)
                {
                    detected.Clear();
                }
                
                return detected;
            }
        }

        private void Start()
        {
            _npcController = GetComponent<NpcController>();
        }

        public Vector3 GetSteeredDirection(Vector3 targetDirection)
        {
            return steerSensor.GetSteeredDirection(targetDirection);
        }

        public void SetSteerTarget(Transform target)
        {
            steerSensor.DestinationTransform = target;
            steerSensor.FaceTowardsTransform = target;
        }
    }
}

