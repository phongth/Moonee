using System;
using Base.Pattern;
using Game;

namespace Base
{
    public class AllyController : NpcController
    {
        private event Action<AllyController> OnAllyTerminated;

        public void SetAttribute(CharacterAttribute attribute)
        {
            _attribute = new CharacterAttribute();
            _attribute.Init(attribute);
        }

        public void AddListener(Action<AllyController> listener)
        {
            OnAllyTerminated += listener;
        }

        public void RemoveListener(Action<AllyController> listener)
        {
            OnAllyTerminated -= listener;
        }

        public override void HealthImpact(float damage)
        {
            base.HealthImpact(damage);

            if (_attribute.healthPoint.StatValue <= 0)
            {
                OnAllyTerminated?.Invoke(this);
                ObjectPooler.Return("Troop", CacheTransform);
            }
        }
    }
}

