using System.Collections;
using System.Collections.Generic;
using Base.MessageSystem;
using Base.Pattern;
using UnityEngine;

namespace Game
{
    public class EnemyController : NpcController
    {
        protected override void Start()
        {
            base.Start();

            targetPosition = -1f * Position;
        }

        public override void HealthImpact(float damage)
        {
            base.HealthImpact(damage);

            if (_attribute.healthPoint.StatValue <= 0)
            {
                ObjectPooler.Return("Enemy", CacheTransform);
                Messenger.RaiseMessage(GameMessage.EnemyEliminated);
            }
        }
    }
}

