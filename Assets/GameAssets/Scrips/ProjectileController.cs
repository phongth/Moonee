using System;
using Base;
using Base.Pattern;
using SensorToolkit;
using UnityEngine;

namespace Game
{
    [RequireComponent(typeof(RaySensor))]
    public class ProjectileController : BaseMono
    {
        [SerializeField] private float speed;
        [SerializeField] private float lifeCycle;

        private Vector3 _direction;
        public Vector3 Direction
        {
            get => _direction;
            set
            {
                _direction = value;
                var angle = Vector3.SignedAngle(CacheTransform.forward, _direction, Vector3.up);
                Rotation = Quaternion.Euler(0, angle, 0);
            }
        }

        private RaySensor _raySensor;
        private float _crrLifeTime;
        
        public float Damage { get; set; }
        public NpcController Source { get; set; }

        private void Start()
        {
            _raySensor = GetComponent<RaySensor>();
            _raySensor.SensorUpdateMode = RaySensor.UpdateMode.Manual;
        }

        private void OnEnable()
        {
            _crrLifeTime = 0;
        }

        private void Update()
        {
            _crrLifeTime += Time.deltaTime;
            if (_crrLifeTime >= lifeCycle)
            {
                ObjectPooler.Return("Projectile", CacheTransform);
                _crrLifeTime = 0;
                return;
            }

            var deltaPos = Direction * speed * Time.deltaTime;
            _raySensor.Length = deltaPos.magnitude;
            _raySensor.Pulse();

            Position += deltaPos;
        }

        public void HitObject(GameObject g, Sensor sensor)
        {
            if (g.CompareTag("Building"))
            {
                var c = g.GetComponent<BuildingController>();
                c.HealthImpact(Damage);
            }
            else if (g.CompareTag("NPC"))
            {
                var c = g.GetComponent<NpcController>();
                if (c.NpcType != Source.NpcType)
                {
                    c.HealthImpact(Damage);
                }
            }
            
            ObjectPooler.Return("Projectile", CacheTransform);
        }
    }
}

