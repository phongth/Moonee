using System;
using System.Collections;
using System.Collections.Generic;
using Base;
using Base.Module;
using NaughtyAttributes;
using UnityEngine;

namespace Game
{
    public class ObjectiveMono : BaseMono
    {
        [SerializeField] private int targetLevel;
        [SerializeField] private ObjectiveType type;
        [SerializeField] private int targetAmount;

        [SerializeField, ReadOnly] private int crrAmount;

        public ObjectiveType ObjectiveType => type;

        public event Action OnCompleted;

        private void OnEnable()
        {
            crrAmount = 0;
        }

        public int TargetAmount
        {
            get => targetAmount;
        }

        public int CrrAmount
        {
            get => crrAmount;
        }

        public void DoObjective()
        {
            crrAmount++;

            if (crrAmount >= targetAmount)
            {
                OnCompleted?.Invoke();
            }
        }
    }

    public enum ObjectiveType
    {
        [StringValue("Building the base")]
        BuildTheBase = 0,
        [StringValue("Create 4 troops")]
        Create4Troops,
        [StringValue("Build the barrack")]
        BuildTheBarrack,
        [StringValue("Build the laboratory")]
        BuildTheLab,
        [StringValue("Defense your base")]
        DefenseYourBase
    }
}

