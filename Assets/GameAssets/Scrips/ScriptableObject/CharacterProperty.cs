using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    [CreateAssetMenu(menuName = "Game/Character Property")]
    public class CharacterProperty : ScriptableObject
    {
        [SerializeField] private CharacterAttribute attribute;

        public CharacterAttribute Attribute => attribute;
    }
    
    [System.Serializable]
    public class CharacterAttribute
    {
        public CharacterType characterType;
        public NpcType npcType;
        public Stat level;
        public Stat healthPoint;
        public Stat attackDamage;
        public Stat loadCapacity;

        public void Init(CharacterAttribute newAttribute)
        {
            characterType = newAttribute.characterType;
            npcType = newAttribute.npcType;
            level = new Stat(newAttribute.level);
            healthPoint = new Stat(newAttribute.healthPoint);
            attackDamage = new Stat(newAttribute.attackDamage);
            loadCapacity = new Stat(newAttribute.loadCapacity);
        }

        public void Upgrade()
        {
            level.Upgrade();
            healthPoint.Upgrade();
            attackDamage.Upgrade();
            loadCapacity.Upgrade();
        }
    }
}

