using System;
using System.Collections;
using System.Collections.Generic;
using Base.Module;
using UnityEngine;

namespace Game
{
    [CreateAssetMenu(menuName = "Game/Building Properties")]
    public class BuildingProperties : ScriptableObject
    {
        [SerializeField] private Attribute attribute;

        public Attribute Attribute
        {
            get => attribute;
            set => attribute = value;
        }

        private void OnEnable()
        {
            attribute ??= new Attribute();
        }

        public void Init(Attribute data)
        {
            attribute = data;
        }

        public void Save()
        {
            SaveLoad.SaveToJson(attribute, name);
        }

        public Attribute Load()
        {
            SaveLoad.LoadFromJson(out Attribute result, name);

            if (result != null)
            {
                return result;
            }

            return null;
        }
    }
    
    [Serializable]
    public class Attribute
    {
        // Basic
        public Stat buildingLevel;
        public Stat buildHealthPoint;
        public Stat productionSpeed;
        // // Input
        // public CurrencyType inputType;
        // public Stat amountIn;
        // public Stat oriAmountIn;

        public Stat amountInRuby;
        public Stat amountInCrystal;
        // Output
        public OutputType outputType;
        public CurrencyType currencyTypeOut;
        public Stat amountOut;
        public Stat oriAmountOut;

        public Attribute() {}

        public event Action OnUpgrade;

        public Attribute(Attribute another)
        {
            buildingLevel = new Stat(another.buildingLevel);
            buildHealthPoint = new Stat(another.buildHealthPoint);
            productionSpeed = new Stat(another.productionSpeed);
            // inputType = another.inputType;
            // amountIn = new Stat(another.amountIn);
            amountInCrystal = new Stat(another.amountInCrystal);
            amountInRuby = new Stat(another.amountInRuby);
            //oriAmountIn = new Stat(another.oriAmountIn);
            outputType = another.outputType;
            currencyTypeOut = another.currencyTypeOut;
            amountOut = new Stat(another.amountOut);
            oriAmountOut = new Stat(another.oriAmountOut);
        }

        public void Upgrade()
        {
            buildingLevel.Upgrade();
            buildHealthPoint.Upgrade();
            productionSpeed.Upgrade();
            // amountIn.Upgrade();
            // oriAmountIn.Upgrade();
            amountOut.Upgrade();
            oriAmountOut.Upgrade();
            
            amountInRuby.Upgrade();
            amountInCrystal.Upgrade();
            
            OnUpgrade?.Invoke();
        }
    }
}

