using System;
using Base;

namespace Game
{
    public class CurrencyItem : BaseMono
    {
        public bool Ignore { get; set; }

        public CurrencyType type;

        public event Action<CurrencyItem> OnInteract;

        public void Interact()
        {
            OnInteract?.Invoke(this);
        }
    }
}

