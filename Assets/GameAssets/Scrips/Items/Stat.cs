using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    [System.Serializable]
    public class Stat
    {
        public string statName;
        [SerializeField] private float statValue;
        public bool isUpgradable;
        public float increment;

        public float StatValue
        {
            get => statValue;
            set => statValue = value;
        }

        public Stat()
        {
        }

        public Stat(Stat source)
        {
            statName = source.statName;
            statValue = source.statValue;
            isUpgradable = source.isUpgradable;
            increment = source.increment;
        }

        public void Upgrade()
        {
            if (isUpgradable)
            {
                statValue += increment;
            }
        }
    }
}

