using System;
using System.Collections;
using System.Collections.Generic;
using Base;
using Base.MessageSystem;
using SensorToolkit;
using TMPro;
using UnityEngine;
using DG.Tweening;

namespace Game
{
    public class UnlockUpgrade : BaseMono
    {
        [SerializeField] private InputCurrencyUi requirementPopup;
        [SerializeField] private List<TriggerSensor> sensors;

        public LockType lockType;
        public int requireCrystal;
        public int requireRuby;

        private int _oriRequireCrystal;
        private int _oriRequireRuby;

        private BuildingController _building;

        private void Awake()
        {
            if (lockType != LockType.Player)
            {
                //_requirementPopup = Instantiate(prefab, GameObject.FindGameObjectWithTag("Main Canvas").transform);
                //requirementPopup.SetTarget(offset, Vector3.zero);
            }
        }

        private void Start()
        {
            Messenger.RegisterListener<LockType>(GameMessage.UnlockUpgrade, OnUnlockUpgrade);
            
            if (lockType != LockType.Player)
            {
                Messenger.RegisterListener<int, CurrencyType>(GameMessage.DropCurrencyUpgrade, OnDropCurrency);
                UpdateText();
                _building = GetComponent<BuildingController>();

                requirementPopup.Active = false;
                sensors.SetActiveAllComponent(false);

                _oriRequireCrystal = requireCrystal;
                _oriRequireRuby = requireRuby;
            }
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            
            Messenger.RemoveListener<LockType>(GameMessage.UnlockUpgrade, OnUnlockUpgrade);
            if (lockType != LockType.Player)
            {
                Messenger.RemoveListener<int, CurrencyType>(GameMessage.DropCurrencyUpgrade, OnDropCurrency);
            }
        }

        private void OnUnlockUpgrade(LockType type)
        {
            if (type == lockType)
            {
                if (type != LockType.Player)
                {
                    sensors.SetActiveAllComponent(true);
                    requirementPopup.Active = true;
                }
                else
                {
                    GetComponent<PlayerController>().LevelUp();
                }
            }
        }

        private void OnDropCurrency(int instanceId, CurrencyType type)
        {
            if (instanceId != InstanceId) return;

            if (type == CurrencyType.Crystal)
            {
                requireCrystal--;
            }

            if (type == CurrencyType.Ruby)
            {
                requireRuby--;
            }
            
            UpdateText();

            if (requireCrystal <= 0 && requireRuby <= 0)
            {
                _building.LevelUp();

                requirementPopup.Active = false;
                
                sensors.SetActiveAllComponent(false);
            }
        }
        
        private void UpdateText()
        {
            string textCurrency = String.Empty;
            if (requireCrystal >= 0)
            {
                textCurrency += $"{requireCrystal} <sprite=\"{Constant.CrystalSpriteAsset}\" index=0>\n";
            }

            if (requireRuby >= 0)
            {
                textCurrency += $"{requireRuby} <sprite=\"{Constant.RubySpriteAsset}\" index=0>";
            }
            requirementPopup.UpdateText(textCurrency);
        }

        public void OnSensorDetected(GameObject obj, Sensor sensor)
        {
            // Sequence t = DOTween.Sequence(requirementPopup.CacheTransform);
            // t.Join(requirementPopup.CacheTransform.DOScale(Vector3.one * 1.25f, .5f));
            // t.PrependInterval(.5f);
            // t.Play();
        }

        public void OnSensorLostDetected(GameObject obj, Sensor sensor)
        {
            // Sequence t = DOTween.Sequence(requirementPopup.CacheTransform);
            // t.Append(requirementPopup.CacheTransform.DOScale(Vector3.one, .5f));
            // t.PrependInterval(.25f);
            // t.Play();
        }
    }
    
    public enum LockType {Player, Headquarter, RubyProduction, TroopBarrack}
}

