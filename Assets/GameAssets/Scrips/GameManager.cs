using System;
using System.Collections;
using System.Collections.Generic;
using Base;
using Base.Module;
using Base.Pattern;
using UnityEngine;

namespace Game
{
    public class GameManager : Singleton<GameManager>
    {
        [SerializeField] private int gameLevel;
        public static int GameLevel
        {
            get
            {
                if (Instance.gameLevel == 0)
                    Instance.gameLevel = 1;

                Instance.gameLevel = Mathf.Clamp(Instance.gameLevel, 1, 5);

                return Instance.gameLevel;
            }

            set => Instance.gameLevel = value;
        }
        public StateParam stateParam = new StateParam();

        public static StateParam StateParam => Instance.stateParam;

        private void Awake()
        {
#if !UNITY_EDITOR
            SaveLoad.LoadFromBinary(out SaveData save, "save.bin");

            gameLevel = save?.gameLevel ?? 0;
            Setting.isVibrate = save?.isVibrate ?? true;
#endif
        }

        private void OnEnable()
        {

            ObjectPooler.InitObjectPool("Projectile");
            ObjectPooler.InitObjectPool("Crystal");
        }

        private void Update()
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    Application.Quit(0);
                }
            }
        }

        private void OnDisable()
        {
            stateParam = null;
        }

        protected override void OnApplicationQuit()
        {
            base.OnApplicationQuit();
#if !UNITY_EDITOR
            SaveData save = new SaveData { gameLevel = this.gameLevel, isVibrate = Setting.isVibrate };
            SaveLoad.SaveToBinary(save, "save.bin");
#endif
        }
    }
    
    public class StateParam
    {
        public bool loadingStateComplete;
        public bool mainStateComplete;
        public bool isAllowInput;
    }
    
}

