using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Game
{
    public class RequirementPopup : OverlayUIManage
    {
        [SerializeField] private TextMeshProUGUI textRequirement;

        public void UpdateText(string text)
        {
            textRequirement.text = text;
        }
    }
}

