using System;
using Base;
using Base.Module;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class NavigationUiController : BaseMono
    {
        [SerializeField] private Transform navigationUiPrefab;
        private Camera _mainCamera;
        private Transform _mainCanvas;
        private Transform _targetTransform;

        private RectTransform _navigationUi;

        private void Awake()
        {
            _mainCamera = Camera.main;
            _mainCanvas = GameObject.FindGameObjectWithTag("Main Canvas").transform;
        }

        private void Start()
        {
            if (GameManager.GameLevel <= 1)
            {
                _navigationUi = Instantiate(navigationUiPrefab, _mainCanvas).GetComponent<RectTransform>();
                _navigationUi.gameObject.SetActive(false);
            }
        }

        private void LateUpdate()
        {
            if (!_navigationUi) return;
            
            Vector3 myPosition = Position.Add(7.5f, Vector3.right + Vector3.forward);
            bool isView = UtilsClass.IsInCameraView(_mainCamera, myPosition);
            _navigationUi.gameObject.SetActive(!isView);
            if (!isView)
            {
                Vector3 newPos = _mainCamera.WorldToScreenPoint(myPosition);
                newPos.x = Mathf.Clamp(newPos.x, _navigationUi.sizeDelta.x / 2f,
                    Screen.width - _navigationUi.sizeDelta.x / 2f);
                newPos.y = Mathf.Clamp(newPos.y, _navigationUi.sizeDelta.y / 2f,
                    Screen.height - _navigationUi.sizeDelta.y / 2f);
                _navigationUi.position = newPos;

                Vector3 targetDirection = (LevelController.Instance.Position - myPosition).normalized;
                Quaternion targetRotation = Quaternion.LookRotation(targetDirection);
                Quaternion result =
                    Quaternion.RotateTowards(_navigationUi.rotation, targetRotation, Time.deltaTime * 8000f);

                result.z = -1f * result.y;
                result.y = 0;

                _navigationUi.rotation = result;
            }
        }

        public void SetTargetTransform(Transform target)
        {
            _targetTransform = target;
        }
    }
}

