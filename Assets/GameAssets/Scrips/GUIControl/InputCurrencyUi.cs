using System.Collections;
using System.Collections.Generic;
using Base;
using UnityEngine;
using TMPro;

namespace Game
{
    public class InputCurrencyUi : BaseMono
    {
        [SerializeField] private TextMeshProUGUI textRequirement;

        public void UpdateText(string text)
        {
            textRequirement.text = text;
        }
    }
}

