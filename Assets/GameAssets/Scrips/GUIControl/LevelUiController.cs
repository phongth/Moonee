using System;
using System.Collections;
using DG.Tweening;
using Base;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class LevelUiController : BaseMono
    {
        [SerializeField] private Image loadingUi;
        [SerializeField] private Text timeTxt;

        private float _crrTime;

        private void OnEnable()
        {
            LoadingAnim().Forget();

            _crrTime = -1f;
            timeTxt.transform.parent.gameObject.SetActive(false);
        }

        private void OnDisable()
        {
            StopCoroutine(CountingTime());
        }

        private async UniTask LoadingAnim()
        {
            loadingUi.color = new Color(0, 0, 0, 255);
            await loadingUi.DOFade(0, 1f).SetEase(Ease.InOutBack).AsyncWaitForCompletion();
        }

        private IEnumerator CountingTime()
        {
            string text = "Enemy Spawn in\n";
            while (_crrTime >= 0)
            {
                _crrTime -= Time.deltaTime;
                int minutes = Mathf.FloorToInt(_crrTime / 60f);
                int seconds = Mathf.FloorToInt(_crrTime % 60f);

                timeTxt.text = text + $"{minutes:00}:{seconds:00}";
                yield return null;
            }

            timeTxt.text = text + "00:00";
        }

        public void SetTimeText(float startTime)
        {
            timeTxt.transform.parent.gameObject.SetActive(true);
            _crrTime = startTime;
            StartCoroutine(CountingTime());
        }

        public void StopTimeText()
        {
            timeTxt.transform.parent.gameObject.SetActive(false);
            StopCoroutine(CountingTime());
            string text = "Enemy Spawn in\n";
            timeTxt.text = text + "00:00";
        }
    }
}

