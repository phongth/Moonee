using System.Collections;
using System.Collections.Generic;
using Base;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class HealthBarManage : OverlayUIManage
    {
        [SerializeField] private Image healthFill;

        public void Fill(float amount)
        {
            healthFill.fillAmount = amount;
        }
    }
}

