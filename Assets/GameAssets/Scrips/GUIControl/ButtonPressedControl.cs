using Base;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class ButtonPressedControl : BaseMono
    {
        enum ButtonEventType {Close, Open}

        [SerializeField] private ButtonEventType eventType;
        [SerializeField] private RectTransform target;
        [SerializeField] private float scaleTo;
        private Button _button;

        private void Awake()
        {
            _button = GetComponent<Button>();
        }

        private void OnEnable()
        {
            _button.onClick.AddListener(OnButtonClick);
        }

        private void OnDisable()
        {
            _button.onClick.RemoveListener(OnButtonClick);
        }

        public void OnButtonClick()
        {
            if (target != null)
            {
                if (eventType == ButtonEventType.Close)
                {
                    DoTweenClose(target, scaleTo).Forget();
                }
                else
                {
                    DoTweenOpen(target, scaleTo).Forget();
                }
            }
            
        }

        private async UniTask DoTweenClose(RectTransform targetT, float toScale)
        {
            Sequence t = DOTween.Sequence(targetT);
            t.Append(targetT.DOScale(toScale, .75f).SetEase(Ease.InCirc));
            t.AppendCallback(() => targetT.gameObject.SetActive(false));
            t.Play();
            await t.AsyncWaitForCompletion();
        }

        private async UniTask DoTweenOpen(RectTransform targetT, float toScale)
        {
            Sequence t = DOTween.Sequence(targetT);
            t.Append(targetT.DOScale(toScale, .75f).SetEase(Ease.OutCirc));
            t.PrependCallback(() => targetT.gameObject.SetActive(true));
            t.Play();
            await t.AsyncWaitForCompletion();
        }
    }
}

