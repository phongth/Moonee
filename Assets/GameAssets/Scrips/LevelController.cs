using System.Collections.Generic;
using Base;
using Base.MessageSystem;
using Base.Pattern;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game
{
    public class LevelController : BaseMono
    {
        private static LevelController _instance;

        public static LevelController Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<LevelController>();
                }

                return _instance;
            }
        }

        [SerializeField] private Stat gameLevel;
        [SerializeField] private List<BuildingController> buildings;
        [SerializeField] private List<ConstructionSpot> constructionSpots;
        [SerializeField] private CameraFollow follow;
        [SerializeField] private ObjectSpawner waveSpawner;
        [SerializeField] private ObjectSpawner crystalSpawner;
        [SerializeField] private LevelUiController mainCanvas;

        private int _numOfEnemy = 0;

        public static float Level => Instance.gameLevel.StatValue;

        private List<ObjectiveMono> _objectives = new List<ObjectiveMono>();
        
        public static ObjectiveMono CurrentObjective => Instance._objectives[(int)Instance.gameLevel.StatValue - 1];

        private void OnEnable()
        {
            gameLevel.StatValue = Mathf.Clamp(GameManager.GameLevel, 1, 5);
            Messenger.RegisterListener(GameMessage.EnemyEliminated, OnEnemyKilled);
            waveSpawner.Spawned += OnWaveSpawned;
        }

        private void Start()
        {
            DOTween.SetTweensCapacity(2000, 500);
            follow.InitOffset();

            Application.targetFrameRate = 60;

            constructionSpots.SetActiveAllMono(false, 1);
            
            _objectives.AddRange(CacheTransform.GetComponentsInChildren<ObjectiveMono>());

            CurrentObjective.OnCompleted += OnObjectiveCompleted;
            
            ActiveConstruction();
        }

        private void LateUpdate()
        {
            follow.FollowTarget();
        }

        private void OnDisable()
        {
            Messenger.RemoveListener(GameMessage.EnemyEliminated, OnEnemyKilled);
            waveSpawner.Spawned -= OnWaveSpawned;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            
            DOTween.KillAll();
            CurrentObjective.OnCompleted -= OnObjectiveCompleted;
            
            buildings.DestroyAll();
            
            _instance = null;
            //Messenger.CleanUp();
        }

        private void OnObjectiveCompleted()
        {
            //gameLevel.Upgrade();
            
            Messenger.RaiseMessage(GameMessage.OnLevelCompleted, true);
            if (gameLevel.StatValue >= 5f)
            {
                mainCanvas.StopTimeText();
                waveSpawner.SetStartTimer(false);
            }
            
            // GameManager.GameLevel = (int) gameLevel.StatValue;
            // GameManager.StateParam.mainStateComplete = true;
        }

        private void ActiveConstruction()
        {
            if (gameLevel.StatValue >= 2 && gameLevel.StatValue <= 3)
            {
                constructionSpots.SetActiveAllMono(true, 1, 3);
            }
            else if (gameLevel.StatValue >= 4)
            {
                constructionSpots.SetActiveAllMono(true, 1);
            }

            if (gameLevel.StatValue > 1)
            {
                constructionSpots[0].ConditionsAreMatch().Forget();
            }
        }

        private void OnWaveSpawned()
        {
            _numOfEnemy = 5;
        }
        
        private void OnEnemyKilled()
        {
            _numOfEnemy -= 1;

            if (_numOfEnemy <= 0)
            {
                waveSpawner.SetStartTimer(true);
                
                mainCanvas.SetTimeText(waveSpawner.timeBetweenWave);
                
                DoObjective();
            }
        }

        public static void DoObjective()
        {
            CurrentObjective.DoObjective();
        }

        public void AddBuilding(BuildingController building)
        {
            buildings.Add(building);
#if !UNITY_EDITOR
            if (Setting.isVibrate)
            {
                Taptic.AndroidTaptic.Vibrate();
            }
#endif

            if (buildings.Find(item => item.GetType() == typeof(Barrack)) != null && gameLevel.StatValue >= 5)
            {
                waveSpawner.SetStartTimer(true);
                
                mainCanvas.SetTimeText(waveSpawner.firstSpawnTime);
            }

            if (building.BuildingType == BuildingType.Headquarter)
            {
                if ((int) gameLevel.StatValue == 1)
                {
                    DoObjective();
                }
            }
            else if (building.BuildingType == BuildingType.TroopBarrack)
            {
                if ((int) gameLevel.StatValue == 2)
                {
                    DoObjective();
                }
            }
            else if (building.BuildingType == BuildingType.Laboratory)
            {
                if ((int) gameLevel.StatValue == 4)
                {
                    DoObjective();
                }
            }
        }

        public void RemoveBuilding(BuildingController building)
        {
            buildings.Remove(building);

            switch (building.BuildingType)
            {
                case BuildingType.Headquarter:
                    constructionSpots[0].Active = true;
                    Messenger.RaiseMessage(GameMessage.OnLevelCompleted, false);
                    break;
                case BuildingType.RubyProduction:
                    constructionSpots[1].Active = true;
                    break;
                case BuildingType.TroopBarrack:
                    constructionSpots[2].Active = true;
                    break;
                case BuildingType.Laboratory:
                    constructionSpots[3].Active = true;
                    break;
            }
        }

        public void SetTimerCrystal(bool isStart)
        {
            crystalSpawner.SetStartTimer(isStart);
        }
    }
}

