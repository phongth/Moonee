using Base.MessageSystem;
using Base.Module;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class UIController : MonoBehaviour
    {
        [SerializeField] private Text objectiveText;
        [SerializeField] private Text objectiveGoal;
        [SerializeField] private Text textLevel;
        [SerializeField] private RectTransform popupMission;
        [SerializeField] private Toggle vibrateToggle;

        [Header("Resource UI")] [SerializeField]
        private GameObject crystalPopup;
        [SerializeField] private TextMeshProUGUI crystalTxt;
        [SerializeField] private GameObject rubyPopup;
        [SerializeField] private TextMeshProUGUI rubyTxt;

        [SerializeField] private GameObject missionComplete;
        [SerializeField] private GameObject missionFailed;

        private ObjectiveMono _crrObjective;

        private void OnEnable()
        {
            _crrObjective = LevelController.CurrentObjective;

            textLevel.text = $"Level {LevelController.Level}";

            vibrateToggle.isOn = Setting.isVibrate;

            Messenger.RegisterListener<int, int>(GameMessage.UpdateResourceUi, OnResourceUi);
            Messenger.RegisterListener<bool>(GameMessage.OnLevelCompleted, OnLevelCompleted);
        }
        private void Start()
        {
            OnLevelLoaded().Forget();

            OnResourceUi(0, 0);
            missionComplete.SetActive(false);
        }

        private void OnDisable()
        {
            Messenger.RemoveListener<int, int>(GameMessage.UpdateResourceUi, OnResourceUi);
            Messenger.RemoveListener<bool>(GameMessage.OnLevelCompleted, OnLevelCompleted);
        }

        private void OnLevelCompleted(bool isWin)
        {
            missionComplete.SetActive(isWin);
            missionFailed.SetActive(!isWin);
            GameManager.StateParam.isAllowInput = false;
        }

        private void OnResourceUi(int crystal, int ruby)
        {
            crystalPopup.SetActive(crystal > 0);
            rubyPopup.SetActive(ruby > 0);

            crystalTxt.text = $"<sprite=\"{Constant.CrystalSpriteAssetUI}\" index=0> {crystal}";
            rubyTxt.text = $"<sprite=\"{Constant.RubySpriteAssetUI}\" index=0> {ruby}";
        }

        private async UniTask OnLevelLoaded()
        {
            Sequence t = DOTween.Sequence(popupMission);
            popupMission.gameObject.SetActive(true);
            t.AppendCallback(() =>
            {
                popupMission.localScale = new Vector3(.1f, .1f, .1f);
            });
            t.Append(popupMission.DOScale(Vector3.one, .75f).SetEase(Ease.InCirc));
            t.Play();
            await t.AsyncWaitForCompletion();
        }

        private void LateUpdate()
        {
            if (_crrObjective)
            {
                objectiveText.text = _crrObjective.ObjectiveType.GetStringValue();
                objectiveGoal.text = $"{_crrObjective.CrrAmount}/{_crrObjective.TargetAmount}";
            }
        }

        public void OnButtonNextClick(bool isWin)
        {
            GameManager.GameLevel += (isWin ? 1 : 0);
            GameManager.StateParam.mainStateComplete = true;
        }

        public void OnCloseClick()
        {
            GameManager.StateParam.isAllowInput = true;
        }

        public void OnVibrateToggle(bool value)
        {
            Setting.isVibrate = value;
        }
    }
}

