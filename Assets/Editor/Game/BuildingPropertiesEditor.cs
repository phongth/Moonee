using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Game
{
    [CustomEditor(typeof(BuildingProperties))]
    public class BuildingPropertiesEditor : Editor
    {
        private BuildingProperties _buildingProperties;

        private SerializedProperty _attribute;

        private SerializedProperty _buildingLevel = null;
        private SerializedProperty _buildingHP = null;
        private SerializedProperty _productionSpeed = null;
        private SerializedProperty _inputType = null;
        
        private SerializedProperty _amountIn = null;
        private SerializedProperty _amountInRuby = null;
        private SerializedProperty _amountInCrystal = null;
        private SerializedProperty _oriAmountIn = null;
        private SerializedProperty _outputType = null;
        private SerializedProperty _currencyOutType = null;
        private SerializedProperty _amountOut = null;
        private SerializedProperty _oriAmountOut = null;

        private void OnEnable()
        {
            _buildingProperties = (BuildingProperties)serializedObject.targetObject;

            _attribute = serializedObject.FindProperty("attribute");
            _buildingLevel = _attribute.FindPropertyRelative("buildingLevel");
            _buildingHP = _attribute.FindPropertyRelative("buildHealthPoint");
            _productionSpeed = _attribute.FindPropertyRelative("productionSpeed");
            _inputType = _attribute.FindPropertyRelative("inputType");
            _amountIn = _attribute.FindPropertyRelative("amountIn");
            _amountInCrystal = _attribute.FindPropertyRelative("amountInCrystal");
            _amountInRuby = _attribute.FindPropertyRelative("amountInRuby");
            _oriAmountIn = _attribute.FindPropertyRelative("oriAmountIn");
            _outputType = _attribute.FindPropertyRelative("outputType");
            _currencyOutType = _attribute.FindPropertyRelative("currencyTypeOut");
            _amountOut = _attribute.FindPropertyRelative("amountOut");
            _oriAmountOut = _attribute.FindPropertyRelative("oriAmountOut");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            
            GUILayout.Label("Basic Properties", EditorStyles.boldLabel);
            EditorGUILayout.PropertyField(_buildingLevel, new GUIContent { text = _buildingLevel.displayName });
            EditorGUILayout.PropertyField(_buildingHP, new GUIContent { text = _buildingHP.displayName}, true);
            EditorGUILayout.PropertyField(_productionSpeed, new GUIContent { text = _productionSpeed.displayName}, true);
            
            GUILayout.Space(5);
            GUILayout.Label("Input", EditorStyles.boldLabel);
            
            // _inputType.enumValueIndex =
            //     EditorGUILayout.IntPopup("Input Type", _inputType.enumValueIndex, _inputType.enumDisplayNames, null, EditorStyles.popup);
            
            EditorGUILayout.PropertyField(_amountInCrystal, new GUIContent { text = _amountInCrystal.displayName }, true);
            EditorGUILayout.PropertyField(_amountInRuby, new GUIContent(text: _amountInRuby.displayName), true);
            
            // if (_buildingProperties.Attribute.inputType != CurrencyType.None)
            // {
            //     if (_buildingProperties.Attribute.inputType != CurrencyType.Both)
            //     {
            //         EditorGUILayout.PropertyField(_amountIn, new GUIContent { text = _amountIn.displayName}, true);
            //         EditorGUILayout.PropertyField(_oriAmountIn, new GUIContent { text = _oriAmountIn.displayName }, true);
            //     }
            //     else
            //     {
            //         
            //     }
            // }

            GUILayout.Space(5);
            GUILayout.Label("Output", EditorStyles.boldLabel);
            _outputType.enumValueIndex = 
                EditorGUILayout.IntPopup("Output Type", _outputType.enumValueIndex, _outputType.enumDisplayNames, null, EditorStyles.popup);
            //_buildingProperties.Attribute.outputType = (OutputType)_outputType.enumValueIndex;
            if (_buildingProperties.Attribute.outputType == OutputType.Currency)
            {
                _currencyOutType.enumValueIndex = 
                    EditorGUILayout.IntPopup("Currency Type", _currencyOutType.enumValueIndex, _currencyOutType.enumDisplayNames, null, EditorStyles.popup);
            }

            if (_buildingProperties.Attribute.outputType != OutputType.None)
            {
                EditorGUILayout.PropertyField(_amountOut, new GUIContent { text = _amountOut.displayName}, true);
                EditorGUILayout.PropertyField(_oriAmountOut, new GUIContent { text = _oriAmountOut.displayName}, true);
            }
            serializedObject.ApplyModifiedProperties();
        }
    }
}

