using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Game
{
    [CustomEditor(typeof(CharacterProperty))]
    public class CharacterPropertyEditor : Editor
    {
        private CharacterProperty _characterProperty;
        private SerializedProperty _attribute;
        private SerializedProperty _characterType;
        private SerializedProperty _npcType;
        private SerializedProperty _level;
        private SerializedProperty _hp;
        private SerializedProperty _attackDamage;
        private SerializedProperty _loadCapacity;

        private void OnEnable()
        {
            _characterProperty = (CharacterProperty)serializedObject.targetObject;
            _attribute = serializedObject.FindProperty("attribute");
            _characterType = _attribute.FindPropertyRelative("characterType");
            _npcType = _attribute.FindPropertyRelative("npcType");
            _level = _attribute.FindPropertyRelative("level");
            _hp = _attribute.FindPropertyRelative("healthPoint");
            _attackDamage = _attribute.FindPropertyRelative("attackDamage");
            _loadCapacity = _attribute.FindPropertyRelative("loadCapacity");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            
            GUILayout.Label("Property", EditorStyles.boldLabel);
            _characterType.enumValueIndex = EditorGUILayout.IntPopup("Character Type", _characterType.enumValueIndex, 
                _characterType.enumDisplayNames, null, EditorStyles.popup);
            if (_characterProperty.Attribute.characterType == CharacterType.Player)
            {
                EditorGUILayout.PropertyField(_loadCapacity, new GUIContent { text = _loadCapacity.displayName }, true);
            }
            else
            {
                _npcType.enumValueIndex = EditorGUILayout.IntPopup("NPC Type", _npcType.enumValueIndex, 
                    _npcType.enumDisplayNames, null, EditorStyles.popup);
                EditorGUILayout.PropertyField(_attackDamage, new GUIContent { text = _attackDamage.displayName }, true);
            }

            EditorGUILayout.PropertyField(_level, new GUIContent { text = _level.displayName }, true);
            EditorGUILayout.PropertyField(_hp, new GUIContent { text = _hp.displayName }, true);

            serializedObject.ApplyModifiedProperties();
        }
    }
}

